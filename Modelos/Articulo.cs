﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Modelos
{
    [Table("Articulo")]
    public class Articulo
    {
        [Key]
        public int Numero { get; set; }

        public String Descripcion { get; set; }

        public void update(Articulo articulo)
        {
            Numero = articulo.Numero;
            Descripcion = articulo.Descripcion;
        }


        public void leerDatos()
        {
            
        }
    }
}
