﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    [Table("PedidoArticulo")]
    public class PedidoArticulo
    {

        [Key, Column(Order = 1), ForeignKey("Pedido")]
        public Guid idPedido { get; set; }
        public Pedido Pedido { get; set; }

        [Key, Column(Order = 2), ForeignKey("Articulo")]
        public int idArticulo { get; set; }
        public Articulo Articulo { get; set; }

        public int Cantidad { get; set; }

        public PedidoArticulo()
        {

        }

        public void leerDatos()
        {

        }

    }
}
