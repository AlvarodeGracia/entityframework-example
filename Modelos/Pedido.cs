﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    [Table("Pedido")]
    public class Pedido
    {
        [Key]
        public Guid IdPedido { get; set;}

        public String numeroCliente { get; set; }

        [Required, ForeignKey("Direccion")]
        public int IdDireccion { get; set; }
        public Direccion Direccion { get; set; }

        [Column(TypeName = "Date")]
        public DateTime ReportDate { get; set; }

        public Pedido()
        {
            numeroCliente = "";
            IdDireccion = 0;
            Direccion = null;
            ReportDate = new DateTime();
        }

        public void update(Pedido pedido)
        {
            IdPedido = pedido.IdPedido;
            numeroCliente = pedido.numeroCliente;
            IdDireccion = pedido.IdDireccion;
            Direccion = pedido.Direccion;
            ReportDate = pedido.ReportDate;


        }

        public void leerDatos()
        {

            

        }
    }
}
