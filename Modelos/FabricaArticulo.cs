﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    [Table("FabricaArticulo")]
    public class FabricaArticulo
    {
        [Key, Column(Order = 1), ForeignKey("Fabrica")]
        public int Id_Fabrica { get; set; }
        public virtual Fabrica Fabrica { get; set; }

        [Key, Column(Order = 2), ForeignKey("Articulo")]
        public int Id_Articulo { get; set; }
        public virtual Articulo Articulo { get; set; }

        public int Cantidad { get; set; }

        public void leerDatos()
        {

        }
    }
}
