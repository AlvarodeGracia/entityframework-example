﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Modelos
{
    [Table("Fabrica")]
    public class Fabrica
    {
       
        [Key]
        public int Numero { get; set; }

        [Required]
        public String TlfContacto { get; set; }
        

        public void update(Fabrica fabrica)
        {
            Numero = fabrica.Numero;
            TlfContacto = fabrica.TlfContacto;
        }

        public void leerDatos()
        {

        }
    }
}
