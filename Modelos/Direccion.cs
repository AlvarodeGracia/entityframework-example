﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tools;

namespace Modelos
{
    [Table("Direccion")]
    public class Direccion
    {
        [Key]
        public int IdDireccion { get; set; }
        public String Numero { get; set; }
        public String Calle { get; set; }
        public String Ciudad { get; set; }
        public String CodigoPostal { get; set; }

        [Required, ForeignKey("Cliente")]
         public String IdCliente { get; set; }
         public virtual Cliente Cliente { get; set; }

        public virtual List<Pedido> Pedidos { get; set; }

        //public String Id { get; set; }
        //[ForeignKey("Id")]
        //public virtual Cliente Cliente { get; set; }

        public Direccion()
        {

           Numero = null;
           Calle = null;
           Ciudad = null;
           CodigoPostal = "";

        }

        public Direccion(Direccion original)
        {

            IdDireccion = original.IdDireccion;
            Numero = original.Numero;
            Calle = original.Calle;
            Ciudad = original.Ciudad;
            CodigoPostal = original.CodigoPostal;
            IdCliente = original.IdCliente;

        }

        public void update(Direccion original)
        {

            IdDireccion = original.IdDireccion;
            Numero = original.Numero;
            Calle = original.Calle;
            Ciudad = original.Ciudad;
            CodigoPostal = original.CodigoPostal;
            IdCliente = original.IdCliente;

        }

        public void leerDatos()
        {

            Numero = Teclado.readString("Escriba el Numero de La direccion");
            Calle = Teclado.readString("Escriba la calle d ela direccion");
            Ciudad = Teclado.readString("Escriba la ciudad d ela direccion");
            CodigoPostal = Teclado.readString("Escriba el CodigoPostal de La direccion");
        }

        public override string ToString()
        {
            string texto = "ID: "+IdDireccion+" Numero: "+Numero+" Calle: "+Calle+" Ciudad: "+Ciudad+" Codigo Postal: "+CodigoPostal;
            return texto;
        }
    }
}
