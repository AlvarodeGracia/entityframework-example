﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tools;

namespace Modelos
{
    [Table("Cliente")]
    public class Cliente
    {
        [Key]
        public String Id { get; set; }
       
        public float Saldo { get; set; }
        public float Credito { get; set; }
        public int Descuento { get; set; }


        //[ForeignKey("Direcciones")]
        //public String Numero { get; set; }
        public virtual List<Direccion> Direcciones { get; set; }


        public Cliente()
        {
            Id = null;
            Saldo = 0;
            Credito = 0;
            Descuento = 0;
            Direcciones = new List<Direccion>();
        }

        public Cliente(Cliente cl)
        {
            Id = cl.Id;
            Saldo = cl.Saldo;
            Credito = cl.Credito;
            Descuento = cl.Descuento;
            Direcciones = new List<Direccion>(cl.Direcciones);
        }

        public void update(Cliente cl)
        {
            Id = cl.Id;
            Saldo = cl.Saldo;
            Credito = cl.Credito;
            Descuento = cl.Descuento;
            Direcciones = new List<Direccion>(cl.Direcciones);
        }

        public void leerDatos()
        {

            setCredito(Teclado.readFloat("Inserte el credito maximo, max: 3.000"));
            Descuento = Teclado.readInt("Inserte el % de descuento");
            Saldo = Teclado.readInt("Inserte el Saldo");
        }

        public void setCredito(float credito)
        {
            if (credito > 3000) credito = 3000;
            Credito = credito;
        }

        public override string ToString()
        {

            string texto = "Id: " + Id + " Saldo: " + Saldo + " Credito: " + Credito + " Descuento: " + Descuento+" \n";
            foreach(Direccion dir in Direcciones)
            {
                texto += dir.ToString()+"\n";
            }
            return texto;
        }
    }
}
