﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools
{
    public class Teclado
    {

        public static String readString(String mensaje)
        {
            Console.WriteLine(mensaje);
            return Console.ReadLine();
        }

        public static float readFloat(String mensaje)
        {
            Console.WriteLine(mensaje);
            if (float.TryParse(Console.ReadLine(), out float variable))
            {
                return variable;
            }
            else
            {
                return 0f;
            }

        }

        public static int readInt(String mensaje)
        {
            Console.WriteLine(mensaje);
            if (int.TryParse(Console.ReadLine(), out int variable))
            {
                return variable;
            }
            else
            {
                return 0;
            }

        }


    }
}
