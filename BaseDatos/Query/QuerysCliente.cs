﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BaseDatos.Query
{
    public class QuerysCliente
    {

        public List<Cliente> filtrar(String ciudad)
        {
            List<Cliente> clientes = null;

            using (var dbConect = new DBConect())
            {
               /* clientes = (from client in dbConect.Clientes
                            join dir in dbConect.Direcciones on client.Id equals dir.IdCliente
                            where dir.Ciudad == ciudad select client).Include(cl => cl.Direcciones).ToList();*/

                clientes = dbConect.Clientes.Include(x => x.Direcciones).Where(c => c.Direcciones.Any( f => f.Ciudad == ciudad)).ToList();

            }
            return clientes;
        }

    }
}
