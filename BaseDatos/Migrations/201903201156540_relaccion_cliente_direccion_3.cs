namespace BaseDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class relaccion_cliente_direccion_3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Direccions", "Cliente_Id", "dbo.Clientes");
            DropIndex("dbo.Direccions", new[] { "Cliente_Id" });
            RenameColumn(table: "dbo.Direccions", name: "Cliente_Id", newName: "Id");
            DropPrimaryKey("dbo.Direccions");
            AlterColumn("dbo.Direccions", "Calle", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Direccions", "Ciudad", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Direccions", "Id", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Direccions", new[] { "Numero", "Calle", "Ciudad" });
            CreateIndex("dbo.Direccions", "Id");
            AddForeignKey("dbo.Direccions", "Id", "dbo.Clientes", "Id", cascadeDelete: true);
            DropColumn("dbo.Clientes", "Numero");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Clientes", "Numero", c => c.String());
            DropForeignKey("dbo.Direccions", "Id", "dbo.Clientes");
            DropIndex("dbo.Direccions", new[] { "Id" });
            DropPrimaryKey("dbo.Direccions");
            AlterColumn("dbo.Direccions", "Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.Direccions", "Ciudad", c => c.String());
            AlterColumn("dbo.Direccions", "Calle", c => c.String());
            AddPrimaryKey("dbo.Direccions", "Numero");
            RenameColumn(table: "dbo.Direccions", name: "Id", newName: "Cliente_Id");
            CreateIndex("dbo.Direccions", "Cliente_Id");
            AddForeignKey("dbo.Direccions", "Cliente_Id", "dbo.Clientes", "Id");
        }
    }
}
