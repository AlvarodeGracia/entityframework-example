namespace BaseDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration_initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        saldo = c.Single(nullable: false),
                        credito = c.Single(nullable: false),
                        descuento = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Direccions",
                c => new
                    {
                        Numero = c.String(nullable: false, maxLength: 128),
                        Calle = c.String(),
                        Ciudad = c.String(),
                        CodigoPostal = c.String(),
                        Cliente_id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Numero)
                .ForeignKey("dbo.Clientes", t => t.Cliente_id)
                .Index(t => t.Cliente_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Direccions", "Cliente_id", "dbo.Clientes");
            DropIndex("dbo.Direccions", new[] { "Cliente_id" });
            DropTable("dbo.Direccions");
            DropTable("dbo.Clientes");
        }
    }
}
