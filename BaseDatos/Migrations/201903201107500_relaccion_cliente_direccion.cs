namespace BaseDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class relaccion_cliente_direccion : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Direccions", new[] { "Cliente_id" });
            AddColumn("dbo.Clientes", "Numero", c => c.String());
            CreateIndex("dbo.Direccions", "Cliente_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Direccions", new[] { "Cliente_Id" });
            DropColumn("dbo.Clientes", "Numero");
            CreateIndex("dbo.Direccions", "Cliente_id");
        }
    }
}
