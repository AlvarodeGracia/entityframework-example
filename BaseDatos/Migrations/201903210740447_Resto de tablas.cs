namespace BaseDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Restodetablas : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Clientes", newName: "Cliente");
            RenameTable(name: "dbo.Direccions", newName: "Direccion");
            RenameTable(name: "dbo.Pedidoes", newName: "Pedido");
            CreateTable(
                "dbo.Articulo",
                c => new
                    {
                        Numero = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.Numero);
            
            CreateTable(
                "dbo.FabricaArticulo",
                c => new
                    {
                        Id_Fabrica = c.Int(nullable: false),
                        Id_Articulo = c.Int(nullable: false),
                        Cantidad = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Id_Fabrica, t.Id_Articulo })
                .ForeignKey("dbo.Articulo", t => t.Id_Articulo, cascadeDelete: true)
                .ForeignKey("dbo.Fabrica", t => t.Id_Fabrica, cascadeDelete: true)
                .Index(t => t.Id_Fabrica)
                .Index(t => t.Id_Articulo);
            
            CreateTable(
                "dbo.Fabrica",
                c => new
                    {
                        Numero = c.Int(nullable: false, identity: true),
                        TlfContacto = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Numero);
            
            CreateTable(
                "dbo.PedidoArticulo",
                c => new
                    {
                        idPedido = c.Guid(nullable: false),
                        idArticulo = c.Int(nullable: false),
                        Cantidad = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.idPedido, t.idArticulo })
                .ForeignKey("dbo.Articulo", t => t.idArticulo, cascadeDelete: true)
                .ForeignKey("dbo.Pedido", t => t.idPedido, cascadeDelete: true)
                .Index(t => t.idPedido)
                .Index(t => t.idArticulo);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PedidoArticulo", "idPedido", "dbo.Pedido");
            DropForeignKey("dbo.PedidoArticulo", "idArticulo", "dbo.Articulo");
            DropForeignKey("dbo.FabricaArticulo", "Id_Fabrica", "dbo.Fabrica");
            DropForeignKey("dbo.FabricaArticulo", "Id_Articulo", "dbo.Articulo");
            DropIndex("dbo.PedidoArticulo", new[] { "idArticulo" });
            DropIndex("dbo.PedidoArticulo", new[] { "idPedido" });
            DropIndex("dbo.FabricaArticulo", new[] { "Id_Articulo" });
            DropIndex("dbo.FabricaArticulo", new[] { "Id_Fabrica" });
            DropTable("dbo.PedidoArticulo");
            DropTable("dbo.Fabrica");
            DropTable("dbo.FabricaArticulo");
            DropTable("dbo.Articulo");
            RenameTable(name: "dbo.Pedido", newName: "Pedidoes");
            RenameTable(name: "dbo.Direccion", newName: "Direccions");
            RenameTable(name: "dbo.Cliente", newName: "Clientes");
        }
    }
}
