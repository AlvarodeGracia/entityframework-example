﻿using Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BaseDatos
{

    public class DBConect : DbContext
    {

        public DBConect() : base("DefaultConnection")
        {

            Database.CommandTimeout = 50000;
            Database.SetInitializer(new InicializadorDbContext());

        }

        public DBConect(string connectionString) : base(connectionString)
        {

            Database.CommandTimeout = 50000;
            Database.SetInitializer(new InicializadorDbContext());

        }

        public System.Data.Entity.DbSet<Cliente> Clientes { get; set; }
        public System.Data.Entity.DbSet<Direccion> Direcciones{ get; set;}
        public System.Data.Entity.DbSet<Fabrica> Fabricas { get; set; }
        public System.Data.Entity.DbSet<Pedido> Pedidos { get; set; }
        public System.Data.Entity.DbSet<Articulo> Articulos { get; set; }
        public System.Data.Entity.DbSet<FabricaArticulo> FabricaArticulo { get; set; }
        public System.Data.Entity.DbSet<PedidoArticulo> PedidoArticulo { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Pedido>().Property(pedido => pedido.IdPedido)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }


        public class InicializadorDbContext : CreateDatabaseIfNotExists<DBConect>
        {
            
            protected override void Seed(DBConect context)
            {

                base.Seed(context);

            }

        }

    }
}
