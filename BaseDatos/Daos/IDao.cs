﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDatos.Daos
{
    public interface IDao <T,K>
    {
        T findById(K key);
        ICollection<T> findAll();
        bool insert(T objeto);
        bool update(T objeto);
        bool delete(T objeto);
    }
}
