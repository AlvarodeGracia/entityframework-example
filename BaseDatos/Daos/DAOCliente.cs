﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace BaseDatos.Daos
{
    public class DAOCliente : IDao<Cliente, String>
    {

        public bool delete(Cliente objeto)
        {
            using (var dbConect = new DBConect())
            {

                Cliente cl = dbConect.Clientes.Where(x => x.Id.Equals(objeto.Id)).SingleOrDefault();
                dbConect.Clientes.Remove(cl);
                dbConect.SaveChanges();
                return true;
            } 
           
        }

        public ICollection<Cliente> findAll()
        {
            ICollection<Cliente> array = null;
            using (var dbConect = new DBConect())
            {
                
                array =  dbConect.Clientes.OrderBy(x=> x.Saldo).ToList();
            }
            return array;

        }

        public Cliente findById(string key)
        {
            Cliente cl = null;

            using (var dbConect = new DBConect())
            {
                //Forzar no Lazy
                //cl = dbConect.Clientes.Include(x => x.Direcciones).Where(x => x.Id.Equals(key)).SingleOrDefault(); 
                //single encuentra 2 peta
                //first o single sin default  null peta
                //cl = dbConect.Clientes.Include(x => x.Direcciones).SingleOrDefault(x => x.Id.Equals(key));
                cl = dbConect.Clientes.Include(x => x.Direcciones).FirstOrDefault(x => x.Id.Equals(key));

                //Concatenar where y la final un To list
            }

            return cl;
        }

        public bool insert(Cliente objeto)
        {
            using (var dbConect = new DBConect())
            {
                dbConect.Clientes.Add(objeto);
                dbConect.SaveChanges();
                return true;
            }
        }

        public bool update(Cliente objeto)
        {
            using (var dbConect = new DBConect())
            {
                Cliente cl = dbConect.Clientes.Where(x => x.Id.Equals(objeto.Id)).SingleOrDefault();
                cl.update(objeto);
                dbConect.SaveChanges();
                return true;
            }

        }
    }
}
