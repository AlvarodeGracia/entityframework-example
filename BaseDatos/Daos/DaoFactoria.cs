﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDatos.Daos
{
    public class DaoFactoria
    {
        public static TipoBBDD tipo = TipoBBDD.ENTITY_FRAMEWORK;

        public static IDao<Cliente, string> getDaoCliente()
        {
            IDao<Cliente, string> dao = null;

            switch (tipo)
            {
                case TipoBBDD.ENTITY_FRAMEWORK:
                    dao = new DAOCliente();
                    break;
            }
            return dao;
        }

        public static IDao<Direccion, int> getDaoDireccion()
        {
            IDao<Direccion, int> dao = null;

            switch (tipo)
            {
                case TipoBBDD.ENTITY_FRAMEWORK:
                    dao = new DaoDireccion();
                    break;
            }
            return dao;
        }
    }

    public enum TipoBBDD
    {
        ENTITY_FRAMEWORK = 0
    }
}
