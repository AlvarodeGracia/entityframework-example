﻿using ArticulosEncargos.OpcionesMenu;
using BaseDatos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace ArticulosEncargos
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();
        }
    }

    class Menu{

        public List<IOpcionMenu> opciones;

        public Menu()
        {
            crearMenu();
            run();
        }

        public void crearMenu()
        {
            opciones = new List<IOpcionMenu>();
            opciones.Add(new OpcionAgregarCliente());
            opciones.Add(new OpcionBuscarCliente());
            opciones.Add(new OpcionDeleteCliente());
            opciones.Add(new OpcionBsucarCiudadCliente());
        }

        public void run()
        {
            bool seguir = true;

            while (seguir)
            {
                int contador = 0;
                foreach(IOpcionMenu opc in opciones)
                {
                    Console.WriteLine(contador +": "+ opc.leer());
                    contador++;
                }

                int opcion = Teclado.readInt("Escriba la opcion: ");
                if (opcion >= 0 || opcion < opciones.Count)
                    opciones.ElementAt(opcion).ejecutar();
            }
        }

        
    }
}
