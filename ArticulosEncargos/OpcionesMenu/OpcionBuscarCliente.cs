﻿using BaseDatos;
using BaseDatos.Daos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace ArticulosEncargos.OpcionesMenu
{
    class OpcionBuscarCliente : IOpcionMenu
    {
        public void ejecutar()
        {

            IDao<Cliente, string> daoCliente = DaoFactoria.getDaoCliente();

            Console.WriteLine("Creando Cliente");

            String idAux = Teclado.readString("Escriba el Id");

            Cliente cl = daoCliente.findById(idAux);

            if (cl == null)
            {
                Console.WriteLine("No se ha encontrado ese Cliente");
            }
            else
            {
                Console.WriteLine(cl.ToString());
            }

           

        }
     

        public string leer()
        {
            return "Buscar Cliente";
        }
    }
}
