﻿using BaseDatos.Daos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace ArticulosEncargos.OpcionesMenu
{
    class OpcionUpdateCliente : IOpcionMenu
    {
        public void ejecutar()
        {
           IDao<Cliente, String> dao = DaoFactoria.getDaoCliente();
           String idAux =  Teclado.readString("Inserte el id del cliente a actualziar");
           Cliente client = dao.findById(idAux);
           if(client != null)
            {
                Console.WriteLine(client.ToString());
                client.leerDatos();
                dao.update(client);
            }
            else
            {
                Console.WriteLine("Ese Cliente no esta en la BBDD");
            }
        }

        public string leer()
        {
            return "Update Cliente";
        }
    }
}
