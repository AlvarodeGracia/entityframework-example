﻿using BaseDatos.Daos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace ArticulosEncargos.OpcionesMenu
{
    class OpcionDeleteCliente : IOpcionMenu
    {
        public void ejecutar()
        {

            IDao<Cliente, string> daoCliente = DaoFactoria.getDaoCliente();
            string idaux = Teclado.readString("Inserte id de cliente a borrar");
            Cliente cl = daoCliente.findById(idaux);
            if(cl != null){
                daoCliente.delete(cl);
            }
            else
            {
                Console.WriteLine("Ese cliente no existe");
            }

        }

        public string leer()
        {
            return "Borrar Cliente Cliente";
        }
    }
}
