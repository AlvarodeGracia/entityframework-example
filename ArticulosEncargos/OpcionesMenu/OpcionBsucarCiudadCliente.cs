﻿using BaseDatos.Query;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticulosEncargos.OpcionesMenu
{
    class OpcionBsucarCiudadCliente : IOpcionMenu
    {
        public void ejecutar()
        {
            QuerysCliente query = new QuerysCliente();

            List<Cliente> clientes = query.filtrar("Madrid").ToList();

            foreach(Cliente c in clientes)
            {
                c.ToString();
            }
        }

        public string leer()
        {
            return "Bsucar por ciudad";
        }
    }
}
