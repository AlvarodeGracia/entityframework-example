﻿using BaseDatos;
using BaseDatos.Daos;
using Modelos;
using System;
using System.Linq;
using Tools;

namespace ArticulosEncargos.OpcionesMenu
{
    class OpcionAgregarCliente : IOpcionMenu
    {

        public void ejecutar()
        {

            IDao<Cliente, string> daoCliente = DaoFactoria.getDaoCliente();
            IDao<Direccion, int> daoDireccion = DaoFactoria.getDaoDireccion();

            Console.WriteLine("Creando Cliente");

            String idAux =  Teclado.readString("Escriba el Id");

            if (daoCliente.findById(idAux) == null)
            {

                var client = new Cliente();
                client.Id = idAux;
                client.leerDatos();

                bool otraDireccion = true;

                while (otraDireccion)
                {
                    int idDireccionAux = Teclado.readInt("Escriba el Numero de La direccion");

                    if (daoDireccion.findById(idDireccionAux) == null)
                    {

                        var direcion = new Direccion();
                        direcion.leerDatos();

                        direcion.IdCliente = client.Id;

                        
                        client.Direcciones.Add(direcion);
                        daoCliente.insert(client);
                       
                        int opcion = Teclado.readInt("Quiere inssertar otra direccion: \n 1-si \n 2-no");
                        otraDireccion = (opcion == 1);
                    }
                    else
                    {
                        Console.WriteLine("Esa Direccion ya esta Registrada");
                        int opcion = Teclado.readInt("Quiere inssertar otra direccion: \n 1-si \n 2-no");
                        otraDireccion = (opcion == 1);

                    }
                }
            }
            else
            {
                Console.WriteLine("Ese Cliente ya Existe");
            }
           
           
        }

        public String leer()
        {
            return "Agregar Cliente";
        }
    }
}
